G-Strings: Gecode with String Variables
=======================================

The source code of G-Strings has moved to:
  https://github.com/ramadini/gecode

The Gecode+S solver used for the experiments of [1] is available at:
  https://bitbucket.org/jossco/gecode-string

The scripts and the benchmarks used for the experiments of [2] are available at:
  https://bitbucket.org/robama/exp_cp_2017

The scripts and the benchmarks used for the experiments of [3] are available at:
  https://bitbucket.org/robama/exp_aaai_2018

The scripts and the benchmarks used for the experiments of [4] are available at:
  https://bitbucket.org/robama/exp_cpaior_2018
  
The scripts and the benchmarks used for the experiments of [5] are available at:
  https://bitbucket.org/robama/exp_cp_2018
  
The scripts and the benchmarks used for the experiments of [6] are available at:
  https://bitbucket.org/robama/exp_cp_2020

References
==========

1. R. Amadini, P. Flener, J. Pearson, J.D. Scott, P.J. Stuckey, G. Tack.
   MiniZinc with Strings. In LOPSTR 2016, Edinburgh, Scotland, UK.

2. R. Amadini, G. Gange, P.J. Stuckey, G. Tack. A Novel Approach to String 
   Constraint Solving. In CP 2017, Melbourne, Victoria, Australia.

3. R. Amadini, G. Gange, P.J. Stuckey. Sweep-based Propagation for String 
   Constraint Solving. In AAAI 2018, New Orleans, Lousiana, USA.

4. R. Amadini, G. Gange, P.J. Stuckey. Propagating lex, find and replace with
   dashed strings. In CPAIOR 2018, Delft, The Netherlands.

5. R. Amadini, G. Gange, P.J. Stuckey. Propagating regular membership with 
   dashed strings. In CP 2018, Lille, France.
   
6. R. Amadini, G. Gange, P.J. Stuckey. Dashed strings and the replace(-all) 
   constraint. In CP 2020, Louvain-la-Neuve, Belgium.
